# Colormaps for HuberSleepLab #

This repository contains colormaps which are perceptually uniform (as much as possible) and therefore better suited for publication. The powerpoint "Huber SleepLab Colormaps.pptx" included lets you quickly browse through the different maps, with little indicators on what each colormap is good for + comments. 

## The Theory ##

Perceptually uniform maps are based on the CIELAB colorspace, which was designed such that every unit of change in the colorspace corresponds to an equally perceptible visual change for humans. It is impossible to "standardize" colors such that they are all equally perceptible, but the focus is rather on the "rate of change" between adjacent colors. 

The main maps (e.g. magma, inferno) created and used in python's matplotlib have the advantage of also being percpetually uniform to different forms of colorblindness, and also percpetually uniform if rendered in black and white.

Unfortunately, the mathematical model defining the CIELAB colorspace is not actually perfectly perceptually uniform, and especially fails to account for changes in brightness (such that changes of near-blacks are much less noticible than changes of near-whites). So the supposedly perceptual uniform colormaps are less uniform along the edges, but still worlds better than the MATLAB defaults. 

### Diverging colormaps ###
While it's nice for plots to be usable in black and white, the reality is that color offers a whole dimention worth exploiting, especially when representing both positive and negative values. Such plots require at least 2 colors, which fade to either white or black (or some common color in the middle). These are known as "diverging" colormaps.

These colormaps cannot be represented in black and white, but can still be constructed to be perceptually uniform. If you really need it to be b/w friendly, just use one of the sequential maps.

It is important to select colors that are not problematic for the colorblind, so avoid red/green. There are some maps that are specifically good for particular types of colorblindness, but this is not really helpful for a publication since they are specific to just one kind of blindness. 



## Practical stuff ##

### How do I use them? ###

1. Clone the repository
2. Either permanently add the path where you've saved them, or write `addpath({folder}/Colormaps/Maps)` in your scripts
3. Change the colormap of a plot by just adding the line `colormap(viridis)` after your plot, replacing 'viridis' with the name of the map you want. 
4. For diverging colormaps: make sure that your 0 value is actually centered in the middle by setting the color axis limits appropriately:
`colormap(rdbu); caxis([-max(abs(Data(:))), max(abs(Data(:)))])`

CET maps need to be used a little differently: `colormap(colorcet('L1'))`, with the string 'L1' changing.

To see which maps are available, look at the Colormaps.pptx slides. The script "run.m" will generate the plots used to compare the colormaps, but it's not commented and currently a mess.

### How do I choose a colormap? ###
The maps provided are all "mathematically" perceptually uniform, but you can use the powerpoint slides to verify which ones look more uniform to your actual human eyes. 

#### Dark vs Light ####
For diverging colormaps, choose the middle value as either light or dark depending on whether you want to emphasize very small differences (white), or hide small differences that are likely insignificant and distracting (dark). 

For "fairness", choose your colormap based on the slides and maybe independent data rather than directly on your data; since there aren't truely perceptually uniform maps, you risk biasing your data presentation by choosing a map that fits best your expectations rather than the reality. (Yes, I'm a hypocrite and made this all with MY data).



### Sources ###

Basic introduction:
https://bids.github.io/colormap/

Perceptually uniform colormaps (viridis, magma, plasma, inferno, cividis):

Ander Biguri (2020). Perceptually uniform colormaps (https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps), MATLAB Central File Exchange. Retrieved April 20, 2020.



Other Matplotlib colormaps were downloaded in python, the matrix saved to a .m file with the same name.



CET maps:
The Center for Exploration and Targating provides a whole set of perceptually uniform maps from here: https://peterkovesi.com/projects/colourmaps/ 

N.B. Their needs were a little different; being also interested in 3D shading, so not all maps are useful for us.

The theory of perceptually uniform colormaps is from: 
Moreland, K. (2009, November). Diverging color maps for scientific visualization. In International Symposium on Visual Computing (pp. 92-103). Springer, Berlin, Heidelberg.

Information specifically regarding divergent maps is in the pdf ColorMapsExpanded_Moreland.pdf

To see if an image is colorblind friendly, you can look at it with this:
https://www.color-blindness.com/coblis-color-blindness-simulator/ 


### Who do I talk to? ###

Sophia Snipes curates this repository. If there's a colormap you want her to add, just ask.

### TODO ###

* Working on a script for automatically creating perceptually uniform colormaps
* Always on the lookout for new maps 
* see which ones are printer friendly