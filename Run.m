
clear
clc
close all
load('topos.mat', 'Topos', 'Chanlocs')
load('TF.mat', 'TF')

%
% Maps = {'parula','jet', 'hot', 'cool', 'spring', 'summer', 'autumn', 'winter', ...
%     'gray', 'bone', 'copper', 'pink', ... % matlab native colors
%     'viridis', 'plasma', 'magma', 'inferno', 'cividis', 'fake_parula', ... % matlab file exchange
%     'BrBG', 'PiYG', 'PRGn', 'PuOr', 'RdBu', 'RdYlBu', 'Spectral', 'twilight', ... % taken from matplotlib
%     'pmkmp','dark_christmas', ... % ?
%     'bluered3', 'france', 'gender', 'gender2', 'hotcold', 'negativeheat', ...
%     'palerainbow', 'pinkgreen', 'tranquil'  % mine
%     };

Maps = {
    'L1', 'L3', 'L5', 'L8', 'L16', 'L17' ...
    'D1', 'D1A', 'D3', 'D4', 'D6', 'D7', 'D13', 'D13',...
    'R1', 'R3'
    
    
    }; % runs "colorecet"

% Maps= {'gender2'};

% peaks
% for Indx = 1:numel(Maps)
%     colormap(colorcet(Maps{Indx}))
% %     colormap(Maps{Indx})
% end

for Indx_F = 1:numel(Maps)
    
    figure( 'units','normalized','outerposition',[0 0 1 1])
    p = panel();
    p.pack(3, 1)
    p(1, 1).pack(1, 4)
    p(2, 1).pack(1, 4)
    
    
    Titles = {'BL', 'S1', 'S2'};
    
    for Indx = 1:3
        p(1, 1, 1, Indx).select()
        
        Lims = [min(Topos(:))-(max(Topos(:))-min(Topos(:))), max(Topos(:))];
        % Lim = max(abs(Topos(:)));
        % Lims = [-Lim, Lim];
        
        topoplot(Topos(:, Indx), Chanlocs, 'maplimits', Lims, 'style', 'map', 'headrad', 'rim', 'gridscale', 150)
        h = colorbar;
        set(h, 'ylim', [min(Topos(:)), max(Topos(:))])
        
        p(2, 1, 1, Indx).select()
        
        
        Change = 100*(Topos(:, Indx) - Topos(:, 1))./Topos(:, 1);
        % Lim = max(abs(Change(:)));
        % Lims = [-Lim, Lim];
        Lims = [-35, 35];
        
        if Indx ==2
            Change = zscore(Change);
            Max = max(abs(Change(:)));
            Lims = [-Max, Max];
            
        end
        
        topoplot(Change, Chanlocs, 'maplimits', Lims, 'style', 'map', 'headrad', 'rim', 'gridscale', 150)
        
        colorbar
        
    end
    
    p(1, 1, 1, 4).select()
    
    XLims = [find(~isnan(mean(TF)), 1, 'first'),...
        find(~isnan(mean(TF)), 1, 'last')];
    TF(isnan(TF)) = 0;
    image(TF, 'CDataMapping', 'scaled')
    xlim(XLims)
    Lim = max(abs(quantile(TF(:), .01)), abs(quantile(TF(:), .99)));
    % caxis([quantile(TF(:), .01), quantile(TF(:), .99)])
    caxis([-Lim Lim])
    colorbar
    set(gca,'visible','off')
    
    
    p(2,1, 1, 4).select()
    imagesc(peaks(300))
    caxis([-8 8])
    colorbar
    
    set(gca,'visible','off')
    
    p(2, 1, 1, 1).select()
    sz = [800,800];  %size of image [y,x], in pixels
    [x,y]= meshgrid(linspace(1,5,sz(2)),linspace(0,1,sz(1)));
    img = sin(exp(x)).*(y.^3);
    img = (img+1)*128;
    imagesc(img);
    caxis([0 255])
    axis equal; axis tight; axis off
    
    
    p(3, 1).select()
    Shift = [ 1, 255:-2:3; 255, 1:2:254; 1:2:255; 3:2:255, 1; 6:2:255, 1:2:5];
    imagesc(Shift)
    axis tight; axis off
    
    
    title(['\fontsize{20}', Maps{Indx_F}])
    colormap(colorcet(Maps{Indx_F}))
    
    
    saveas(gcf, fullfile('Maps',[Maps{Indx_F}, '.svg']))
    
end

